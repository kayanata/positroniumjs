"use strict";
/* purpose of this library: "serve as 'reacty' way of working in non-DOM scripts" */
/* current version: 0.1f-a branch 0.2 */
var positronium = positronium || {};

positronium.execute = (callback, method, args) => {
  /* completly unnecesary and unreadable ternary operator. but i love it! */
  this.global_timer = method ? (args ? setInterval(() => method(callback(), ...args), 100) : setInterval(() => method(callback()), 100)) : setInterval(() => callback(), 100);
};

positronium.stop = () => {
  clearInterval(this.global_timer);
};

class _Component {
  constructor() {
    this.state = {};
    /* _cstate: copy of this.state - used for setting it */
    this._cstate = {};
    /* inherited: contains properties of mounted components */
    this.inherited = {};
  };
  set_state(object) {
    /* adds passed value to the this.state */
    this._cstate = this.state;
    const keys = Object.keys(object);
    const values = Object.values(object);
    for (var i = keys.length - 1; i >= 0; i--) {
      this._cstate[keys[i]] = values[i];
    };
    this.state = this._cstate;
  };
  mount(component) {
    if(component.state.mountable) {
      if(component.state.mountable == false) {
        throw new Error("Component is unmountable");
      };
    };
    if(component.component_will_mount) {
      component.component_will_mount();
    };
    this.inherited[component.constructor.name] = component.state;
    if(component.component_did_mount) {
      component.component_did_mount();
      return 0;
    } else {
      return -1;
    };
  };
  unmount(component) {
    if(component.component_will_unmount) {
      component.component_will_unmount();
    };
    delete this.inherited[component.constructor.name];
    if(component.component_did_unmount) {
      component.component_did_unmount();
      return 0;
    } else {
      return -1;
    };
  };
};
/* assign _Component to be part of positronium */
positronium.Component = _Component;